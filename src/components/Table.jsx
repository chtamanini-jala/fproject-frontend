import React, { useState } from "react";

import useTable from "../hooks/useTable";
import "./Table.css";
import TableFooter from "./TableFooter";

const Table = ({ data, rowsPerPage }) => {
  const [page, setPage] = useState(1);
  const { slice, range } = useTable(data, page, rowsPerPage);
  return (
    <>
      <table className="table">
        <thead className="tableRowHeader">
          <tr>
          <th className="tableHeader">Id</th>
            <th className="tableHeader">Username</th>
            <th className="tableHeader">Type</th>
          </tr>
        </thead>
        <tbody>
          {slice.map((el) => (
            <tr className="tableRowItems" key={el.id}>
              <td className="tableCell">{el.id}</td>
              <td className="tableCell">{el.login}</td>
              <td className="tableCell">{el.type}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <TableFooter range={range} slice={slice} setPage={setPage} page={page} />
    </>
  );
};

export default Table;
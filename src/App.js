import React, { useState, useEffect } from "react";
import "./App.css";
import Table from "./components/Table";

const App = () => {
  const [users, setUsers] = useState([]);
  const [rows, setRows] = useState(8);

  useEffect(() => {
    fetch(`https://api.github.com/users?since=165`)
      .then(response => response.json())
      .then(data => {
        console.log(data);
        setUsers(data);
      });
  }, []);

  return (
    <main >
      <div>
        <ul>
          <li><a href="#">Users</a></li>
        </ul>
      </div>
      <div className="container">
        <div className="wrapper">
          <input type="text" placeholder="Rows" onChange={e => setRows(e.target.value)} />
          <Table data={users} rowsPerPage={rows} />
        </div>
      </div>
    </main>
  );
};

export default App;
